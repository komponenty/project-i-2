/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.idzdowiezienia;
import com.mycompany.idzdowiezienia.contracts.*;
import com.mycompany.pole.Pole;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class IdzDoWiezienia extends Pole implements IAresztuj {
    public IdzDoWiezienia() {
        super();
        this.ikona=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/idzdowiezienia/grafika/aresztowany.png")).getImage();;
        this.tytul="Idź do więzienia";
        this.opis="Przejdź prosto do więzienia, nie przechodź przez START, nie pobieraj pensji";

    } 
    
    
}
