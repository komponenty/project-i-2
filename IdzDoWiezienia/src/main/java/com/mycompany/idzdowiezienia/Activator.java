package com.mycompany.idzdowiezienia;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import com.mycompany.pole.*;

public class Activator implements BundleActivator {
    private IdzDoWiezienia idz;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.idz=new IdzDoWiezienia();
        this.context=context;
        context.registerService(Pole.class.getName(), this.idz, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
