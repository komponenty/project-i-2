/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.instrukcja;

import com.mycompany.instrukcja.contracts.IPomoc;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Joanna667
 */
public class Instrukcja extends JFrame implements IPomoc{
    private List<String> info;
    public Instrukcja(){
        this.setPreferredSize(new Dimension(300,400));
        this.info=new ArrayList<String>(){};
    }
    public List<String> getLista() {
        return info;
    }
    public void pokaz(){
        this.setLayout(new GridLayout(1,this.info.size()));
        for (int i = 0; i < this.info.size(); i++) {
            JLabel in = new JLabel();
            in.setSize(200, 20);
            in.setText(info.get(i));
            in.setVisible(true);
            this.add(in);
        }
        this.setSize(200, 600);
       this.setVisible(true);
    }

    public void dodajInfo(String s) {
       this.info.add(s);
    }

    public void setLista(List<String> lista) {
        this.removeAll();
        this.info=lista;
    }
    
}
