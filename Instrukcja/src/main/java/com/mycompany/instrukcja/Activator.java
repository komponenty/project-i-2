package com.mycompany.instrukcja;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Instrukcja instrukcja;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
       this.instrukcja=new Instrukcja();
       this.context=context;
       context.registerService(Instrukcja.class.getName(), this.instrukcja, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
