/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.instrukcja.contracts;

import java.util.List;

/**
 *
 * @author Joanna667
 */
public interface IPomoc {
    List<String> getLista();
    void dodajInfo(String s);
    void setLista(List<String> lista); 
    void pokaz();
}
