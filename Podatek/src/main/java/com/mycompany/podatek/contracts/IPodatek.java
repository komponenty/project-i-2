/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.podatek.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.pole.contract.IPole;

/**
 *
 * @author Joanna667
 */
public interface IPodatek extends IPole {
    int getKwota();
    void zaplac(IGracz g);
}