/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.podatek;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.podatek.contracts.IPodatek;
import com.mycompany.pole.Pole;
import java.awt.Image;


/**
 *
 * @author Joanna667
 */
public class Podatek extends Pole implements IPodatek {
    private int kwota;
    private boolean zalatwione;
    public Podatek(Image ikona, int kwota) {
        super();
        this.kwota=kwota;
        this.ikona=ikona;
        this.tytul="Podatek dochodowy";
    }

    public int getKwota() {
        return kwota;
    }
//to tez wlasciwie mógłby być setter, ale nazwa zaplac jest bardziej intuicyjna
    //dla gracza wywołujemy tę metodę w czasie sciągania z niego forsy
    //niby forsa idzie do banku ale bank ma niesk ilość pieniedzy wiec ta metoda nie musi operowac na banku
    public void zaplac(IGracz gracz) {

       int forsa = gracz.getSrodki();
       gracz.setSrodki(forsa-kwota);
       this.zalatwione=true;
    }
}
