package com.mycompany.podatek;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import com.mycompany.pole.Pole;

public class Activator implements BundleActivator {
    private Podatek podatek;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.podatek=new Podatek(null,0);
        this.context=context;
        context.registerService(Pole.class.getName(), this.podatek, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
