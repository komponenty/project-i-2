package com.mycompany.gracz2;

import com.mycompany.gracz.Gracz;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Gracz2 komponent;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
       this.komponent=new Gracz2();
        this.context=context;
        context.registerService(Gracz.class.getName(), this.komponent, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
