/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz2;

import com.mycompany.gracz.Gracz;
import com.mycompany.pionek.Pionek;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Gracz2 extends Gracz{
    public Gracz2(){
        super();
        this.imie="Gracz2";
        Image ikonka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/gracz2/pionek2.png")).getImage();
        this.pionek.setIkonka(ikonka);
        this.pionek.setNazwa("2");
        this.pionek.setNumer(2);
        
    }
}
