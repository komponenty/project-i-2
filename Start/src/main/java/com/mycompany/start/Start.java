/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.start;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.pole.Pole;
import com.mycompany.start.contracts.IStart;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Start extends Pole implements IStart {
    private boolean pensjapobrana;
    private int pensja;
    public Start() {
        super();
             this.tytul="START. Pobierz pensję";
       this.ikona=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/start/grafika/start1.png")).getImage();
    }

    public void PobierzPensje(IGracz g) {
        int forsa = g.getSrodki();
        //tu będzie sprawdzane czy gracz nie jest bankrutem
        g.setSrodki(forsa+200);
        this.pensjapobrana=true;
    }

    @Override
    public int getPensja() {
        return pensja;
    }

    @Override
    public void setPensja(int p) {
        this.pensja=p;
    }
    
}
