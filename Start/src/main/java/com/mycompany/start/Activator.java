package com.mycompany.start;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import com.mycompany.pole.*;

public class Activator implements BundleActivator {
    private Start start;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.start=new Start();
        this.context=context;
        context.registerService(Pole.class.getName(), this.start, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
