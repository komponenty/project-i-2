/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.start.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.pole.contract.IPole;


/**
 *
 * @author st
 */
public interface IStart extends IPole {
    void PobierzPensje(IGracz g);
    int getPensja();
    void setPensja(int p);
}
