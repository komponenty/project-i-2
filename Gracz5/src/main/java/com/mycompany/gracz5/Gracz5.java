/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz5;

import com.mycompany.gracz.Gracz;
import com.mycompany.pionek.Pionek;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Gracz5 extends Gracz{
    public Gracz5(){
        super();
        this.imie="Gracz5";
        Image ikonka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/gracz5/pionek5.png")).getImage();
        this.pionek.setIkonka(ikonka);
        this.pionek.setNazwa("5");
        this.pionek.setNumer(5);
        
    }
}
