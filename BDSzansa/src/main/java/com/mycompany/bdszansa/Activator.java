package com.mycompany.bdszansa;

import com.mycompany.bazakart.Baza;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Szanse szanse;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.szanse=new Szanse();
        this.context=context;
        context.registerService(Baza.class.getName(), this.szanse, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
