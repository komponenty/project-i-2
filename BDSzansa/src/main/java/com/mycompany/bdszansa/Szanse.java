/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.bdszansa;

import com.mycompany.bazakart.Baza;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Szanse extends Baza {
    public Szanse(){
        super();
        this.ikona=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/bdszansa/grafika/szansa.png")).getImage();
        this.nazwa="SZANSA";
        //troszkę uproszczone
        this.karty[0].setOpis("Zostałeś obciążony kosztami napraw. Zapłać 115zł");
        this.karty[0].setKwota(-115);
        this.karty[0].setRuch(50);
        this.karty[1].setOpis("Idz do przodu o trzy pola");
        this.karty[1].setKwota(0);
        this.karty[1].setRuch(3);
        this.karty[2].setOpis("Przeprowadź generalny remont wszystkich budynków. Zapłać 250zł");
        this.karty[2].setKwota(-100);
        this.karty[2].setRuch(50);
        this.karty[3].setOpis("Przejdź na Aleje Ujazdowskie");
        this.karty[3].setKwota(0);
        this.karty[3].setRuch(39);
        this.karty[4].setOpis("Przejdź na Ulicę Płowiecką. Jeśli miniesz po drodze START, pobierz 200zł");
        this.karty[4].setKwota(0);
        this.karty[4].setRuch(11);
        this.karty[5].setOpis("Przejdź na Plac Wilsona. Jeśli miniesz po drodze START, pobierz 200zł");
        this.karty[5].setKwota(0);
        this.karty[5].setRuch(24);
        this.karty[6].setOpis("Otrzymałeś kredyt budowlany. Pobierz 200zł");
        this.karty[6].setKwota(200);
        this.karty[6].setRuch(50);
        this.karty[7].setOpis("Wyjdź bezpłatnie z więzienia.");
        this.karty[7].setKwota(666);
        this.karty[7].setRuch(50);
        this.karty[8].setOpis("Przejdź na Dworzec Gdański. Jeśli miniesz po drodze START, pobierz 200zł");
        this.karty[8].setKwota(0);
        this.karty[8].setRuch(15);
        this.karty[9].setOpis("Wygrałeś konkurs krzyżówkowy. Pobierz 100zł");
        this.karty[9].setKwota(100);
        this.karty[9].setRuch(50);
        this.karty[10].setOpis("Bank wypłaca Ci dywidendę w wysokości 50zł");
        this.karty[10].setKwota(50);
        this.karty[10].setRuch(50);
        this.karty[11].setOpis("Mandat za przekroczenie prędkości. Zapłać 15zł");
        this.karty[11].setKwota(-15);
        this.karty[11].setRuch(50);
        this.karty[12].setOpis("GRZYWNA! Zapłać 20zł");
        this.karty[12].setKwota(-20);
        this.karty[12].setRuch(50);
        this.karty[13].setOpis("Zapłać za szkołę, 150zł");
        this.karty[13].setKwota(-150);
        this.karty[13].setRuch(50);
        this.karty[14].setOpis("Idź do więzienia. Przejdź prosto do więzienia! Nie przechodź przez START");
        this.karty[14].setKwota(0);
        this.karty[14].setRuch(30);
        this.karty[15].setOpis("Przejdź na START");
        this.karty[15].setKwota(0);
        this.karty[15].setRuch(0);
    }
    @Override
    public int getImmunitet() {
        if(wybrana.getKwota()!=666)
           return 0;
        if (wybrana.getRuch()==30) {
            return -1;
        }
        return 1;
    }
}