/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.karta;

import com.mycompany.karta.contract.*;

/**
 *
 * @author Joanna
 */
public class Karta implements IKarta {
    private String opis;
    private int kwota;
    private int ruch;
    public Karta(String opis, int kwota, int ruch){
        this.kwota=kwota;
        this.opis=opis;
        this.ruch=ruch;
    }
    public String getOpis() {
        return opis;
    }

    public void setOpis(String text) {
        this.opis=text;
    }

    public int getKwota() {
        return kwota;
    }

    public void setKwota(int kwota) {
        this.kwota=kwota;
    }

    public int getRuch() {
       return ruch;
    }

    public void setRuch(int cel) {
        this.ruch=cel;
    }
    
}
