/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.karta.contract;

/**
 *
 * @author Joanna
 */
public interface IKarta {
    String getOpis();
    void setOpis(String text);
    int getKwota();
    void setKwota(int kwota);
    int getRuch();
    void setRuch(int cel);
    
    
}
