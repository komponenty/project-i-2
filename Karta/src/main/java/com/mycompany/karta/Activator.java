package com.mycompany.karta;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Karta karta;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.karta=new Karta("", 0, 0);
        this.context=context;
        context.registerService(Karta.class.getName(), this.karta, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
