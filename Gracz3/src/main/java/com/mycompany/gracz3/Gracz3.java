/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz3;

import com.mycompany.gracz.Gracz;
import com.mycompany.pionek.Pionek;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Gracz3 extends Gracz{
    public Gracz3(){
        super();
        this.imie="Gracz3";
        Image ikonka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/gracz3/pionek3.png")).getImage();
        this.pionek.setIkonka(ikonka);
        this.pionek.setNazwa("3");
        this.pionek.setNumer(3);
        
    }
}
