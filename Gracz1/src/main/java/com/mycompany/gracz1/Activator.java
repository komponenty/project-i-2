package com.mycompany.gracz1;

import com.mycompany.gracz.Gracz;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Gracz1 komponent;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
       this.komponent=new Gracz1();
        this.context=context;
        context.registerService(Gracz.class.getName(), this.komponent, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
