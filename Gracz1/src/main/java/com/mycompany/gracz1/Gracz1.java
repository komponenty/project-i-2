/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz1;

import com.mycompany.gracz.Gracz;
import com.mycompany.pionek.Pionek;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Gracz1 extends Gracz{
    public Gracz1(){
        super();
        this.imie="Gracz1";

        Image ikonka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/gracz1/pionek1.png")).getImage();
        this.pionek.setIkonka(ikonka);
        this.pionek.setNazwa("1");
        this.pionek.setNumer(1);
        
    }
}
