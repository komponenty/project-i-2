/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.dzielnica;

import com.mycompany.dzielnica.contracts.IDzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import java.awt.Color;
import java.beans.PropertyChangeSupport;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Joanna667
 */
public class Dzielnica implements IDzielnica {
    private List<IDzialka> dzialki;
    private String wlasciciel;
    private int level;
    private Color kolor;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Dzielnica(){
        this.wlasciciel=null;
    }
    public List<IDzialka> getDzialki() {
        return dzialki;
    }

    public void setDzialki(List<IDzialka> dzialka) {
       this.dzialki=dzialka;
    }


    public int getLevel() {
        for (int i = 1; i < dzialki.size(); i++) {
            if (dzialki.get(i).getLevel()!=dzialki.get(i-1).getLevel()) {
                return dzialki.get(i).getLevel(); 
            }
            
        }
        return dzialki.get(0).getLevel();
                
       
    }

    public void setLevel(int i) {
        for (int j = 0; j < dzialki.size(); j++) {
            int a = dzialki.get(j).getLevel();
            if ((level-a)<=1){
                this.level=i;
            } else {
                JOptionPane.showMessageDialog(null, "Błąd! Najpierw należy postawić na wszystkich działkach po "+level+" domów");
            }
        }
       this.level=i;
    }

    public Color getColor() {
        return kolor;
    }

    public void setColor(Color c) {
       this.kolor=c;
    }

    public String getWlasciciel() {
           return wlasciciel;

    }

    public void setWlasciciel(IGracz g) {
        this.wlasciciel=g.getImie();
    }
    
}
