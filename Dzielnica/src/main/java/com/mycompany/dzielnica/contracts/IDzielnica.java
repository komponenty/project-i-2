/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.dzielnica.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import java.awt.Color;
import java.util.List;

/**
 *
 * @author Joanna667
 */
public interface IDzielnica {
    List<IDzialka> getDzialki();
    void setDzialki(List<IDzialka> dzialka);
    int getLevel();
    void setLevel(int i);
    Color getColor();
    void setColor(Color c);
    String getWlasciciel();
    void setWlasciciel(IGracz g);
}
