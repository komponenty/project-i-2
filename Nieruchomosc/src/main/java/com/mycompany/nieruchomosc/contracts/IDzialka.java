/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.nieruchomosc.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pole.contract.IPole;
import java.awt.Color;

/**
 *
 * @author st
 */
public interface IDzialka extends INieruchomosc {
    int getCd();
    void setCzynsz(int kwota);
    int getCzynsz(int level);
    void PostawDom();
   // void SprzedajDom(Budynek b);
    Color getColor();
    boolean getStatus();
    
}
