package com.mycompany.nieruchomosc;

import java.awt.Color;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import com.mycompany.pole.*;

public class Activator implements BundleActivator {
    private Nieruchomosc nieruchomosc;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.nieruchomosc=new Nieruchomosc(0,"",0,Color.GRAY);
        this.context=context;
        context.registerService(Pole.class.getName(), this.nieruchomosc, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
