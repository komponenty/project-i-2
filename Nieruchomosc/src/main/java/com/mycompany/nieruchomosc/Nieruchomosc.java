/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.nieruchomosc;

import com.mycompany.budynek.Budynek;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.Obiekt;
import com.mycompany.pole.Pole;
import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author Joanna667
 */
public final class Nieruchomosc extends Obiekt implements IDzialka {
    private Color kolor;
    private int cenadomu;
    private int hipoteka;
    private int idKarty;
    private int[] czynsz;
    public Nieruchomosc(int cena, String tytul, int cd, Color grupa){
        super(cena, tytul);
        this.level=0;
        this.tytul=tytul;
        this.cena=cena;
        this.cenadomu=cd;
        this.kolor=grupa;
        this.hipoteka=cena/2;
        this.czynsz=new int[6];
        
    }
    public void PostawDom() {
        if(level<4){
          this.level+=1;
        }
        else if(level==4){
            this.level+=1;
        }
        else{
            this.level+=0;
            JOptionPane.showMessageDialog(null, "Maksymalny poziom zabudowania został osiągnięty!");
        }
          
    }
    

 
    public void setKolor(Color k){
        this.kolor=k;
    }

    @Override
    public void setCzynsz(int kwota) {
        this.czynsz[0]=kwota;
        this.czynsz[1]=5*kwota;
        this.czynsz[2]=3*czynsz[1];
        this.czynsz[3]=2*czynsz[2];
        this.czynsz[4]=2*czynsz[3];
        this.czynsz[5]=czynsz[4]+100;
        
    }

   public int getCzynsz(int level) {
        if(level <=5)
          return czynsz[level];
        else
          return czynsz[5];
    }
    public int getCd(){
        return cenadomu;
    }

    @Override
    public void zakup(IGracz g) {
       this.wlasciciel=g.getImie();
    }

    @Override
    public void sprzedaj(IGracz g) {
        if (level==0 && zastawiona==false) {
            this.wlasciciel=g.getImie();
        }
        else{
            
        }
    }
    @Override
    public void oddaj(){
        this.wlasciciel=null;
        
    }


    public void SprzedajDom() {
        if (level==0) {
            this.level=0;
            JOptionPane.showMessageDialog(null, "Nie ma jeszcze co sprzedać!");
        }
        else{
          this.level-=1;
        }
    }

    @Override
    public int getLevel() {
        return level;
    }

    public Color getColor() {
        return kolor;
    }

    public int getIdKarty() {
        return idKarty;
    }

    public void setIdKarty(int i) {
        this.idKarty=i;
    }

    @Override
    public boolean getStatus() {
       return zastawiona;
    }
    
 


}






  

    

    

    

   
    

