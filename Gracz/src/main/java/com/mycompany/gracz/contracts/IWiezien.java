/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz.contracts;

/**
 *
 * @author Joanna667
 */
public interface IWiezien {
    int getKara();
    void setKara(int i);
    boolean getStatus();
    void setStatus(boolean b);
}
