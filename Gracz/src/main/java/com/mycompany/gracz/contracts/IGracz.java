/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz.contracts;

import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pionek.contracts.IPionek;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 *
 * @author Joanna667
 */
public interface IGracz {
    IPionek getPionek();
    void dodajNieruchomosc(IDzialka d);
    void sprzedajNieruchomosc(IDzialka d);
    void dodajObiekt(INieruchomosc n);
    void sprzedajObiekt(INieruchomosc n);
    void setSrodki(int i);
    int getSrodki();
    String getImie();
    void setImie(String s);
    boolean sprawdz();
    List<Dzielnica> getNieruchomosci();
    List<INieruchomosc> getObiekty();
    void setImmunitet(int i);
    int getImmunitet();
    void setMonopol(Dzielnica d);
    void addPropertyChangeListener(PropertyChangeListener listener);
    void removePropertyChangeListener(PropertyChangeListener listener);
}
