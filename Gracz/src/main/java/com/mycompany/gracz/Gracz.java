/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz;

//import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.gracz.contracts.IWiezien;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pionek.Pionek;
import com.mycompany.pionek.contracts.IPionek;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joanna667
 */
public class Gracz implements IGracz, IWiezien{
    protected int srodki;
    protected List<Dzielnica> nieruchomosci;
    protected List<INieruchomosc> obiekty;
    protected IPionek pionek;
    protected String imie;
    protected boolean aktywny;
    protected boolean wolny;
    protected boolean wykonalRuch;
    protected int immunitet;
    protected int kara;
    protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Gracz(){
        this.srodki=1500;
        this.imie="";
        this.aktywny=true;
        this.wolny=true;
        this.immunitet=0;
        this.nieruchomosci=new ArrayList<Dzielnica>(){};
        this.obiekty=new ArrayList<INieruchomosc>(){};
        this.pionek=new Pionek();
    }

    public void dodajNieruchomosc(IDzialka d) {
        List<Dzielnica> lista = this.nieruchomosci;
        List<INieruchomosc> lista1 = this.obiekty;
        for (int i = 0; i < nieruchomosci.size(); i++) {
            Dzielnica dz = nieruchomosci.get(i);

                if(dz.getDzialki().get(0).getColor()==d.getColor()){
                dz.getDzialki().add(d);
                this.obiekty.add(d);
                this.pcs.firePropertyChange("nieruchomosci", lista, this.nieruchomosci);
                this.pcs.firePropertyChange("nieruchomosci", lista1, this.obiekty);
                return;
            }
        }
        Dzielnica nowa = new Dzielnica();
        nowa.setDzialki(new ArrayList<IDzialka>(){});
        nowa.getDzialki().add(d);
        this.nieruchomosci.add(nowa);
        this.obiekty.add(d);
    }



    public void setSrodki(int i) {
       this.srodki=i;
    }

    public int getSrodki() {
       return srodki;
    }

    public void dodajObiekt(INieruchomosc n) {
       this.obiekty.add(n);
    }

    public void sprzedajObiekt(INieruchomosc n) {
       obiekty.remove(n);
    }

    public boolean sprawdz() {
        int wartoscDzialek = 0;
       
       for (int i = 0; i < nieruchomosci.size(); i++) {
            Dzielnica d = nieruchomosci.get(i);
           // List<INieruchomosc>
           for (IDzialka n:d.getDzialki()) {
              //INieruchomosc n = d.getDzialki().get(i);
              wartoscDzialek+=n.getHipoteka();
           }
        }
        for (int i = 0; i < obiekty.size(); i++) {
            wartoscDzialek+=obiekty.get(i).getHipoteka();
        }
        //BANKRUT!
        if (srodki<=0 && Math.abs(srodki)>wartoscDzialek) {
           aktywny=false;
           this.pcs.firePropertyChange("aktywny", true, false);
        }
       return aktywny;
    }

    public IPionek getPionek() {
       return pionek;
    }

    public List<Dzielnica> getNieruchomosci() {
       return nieruchomosci;
    }
    public List<INieruchomosc> getObiekty() {
       return obiekty;
    }
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.removePropertyChangeListener(listener);
    } 

    @Override
    public void setImie(String s) {
       String st=this.imie;
       this.imie=s;
       this.pcs.firePropertyChange("imie", st, s);
    }

    @Override
    public boolean getStatus() {
        return wolny;
    }

    @Override
    public void setStatus(boolean b) {
       Boolean b1=this.wolny;
       this.wolny=b;
       this.pcs.firePropertyChange("wolny", (Object)b1, (Object)b);
    }

    @Override
    public void sprzedajNieruchomosc(IDzialka d) {
         List<Dzielnica> lista = this.nieruchomosci;
        for (int i = 0; i < nieruchomosci.size(); i++) {
            Dzielnica dz = nieruchomosci.get(i);
            if (dz.getDzialki().contains(d)) {
                dz.getDzialki().remove(d);
                this.pcs.firePropertyChange("nieruchomosci", lista, this.nieruchomosci);
            }
        }
    }

    public String getImie() {
        return imie;
    }
    public void setMonopol(Dzielnica d) {
        List<IDzialka> lista = d.getDzialki();
        for (int i = 0; i < lista.size(); i++) {
            if(lista.get(i).getWlasciciel().isEmpty())
                return;
            if (!lista.get(i).getWlasciciel().equals(imie)) {
               return;
            }
        }
        d.setWlasciciel(this);
        
    }

    public void setImmunitet(int i) {
        this.immunitet=i;
    }

    public int getImmunitet() {
        return immunitet;
    }

//    public boolean getRuch() {
//        return wykonalRuch;
//    }
//
//    public void setRuch(boolean b) {
//        this.wykonalRuch=b;
//    }

    public int getKara() {
        return kara;
    }

    public void setKara(int i) {
        this.kara=i;
    }
}
