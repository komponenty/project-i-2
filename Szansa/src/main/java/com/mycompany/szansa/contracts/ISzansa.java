/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.szansa.contracts;

import com.mycompany.pole.contract.IPole;

/**
 *
 * @author Joanna
 */
public interface ISzansa extends IPole {
    String getOpis();
    void setOpis(String opis);
    int getRuch();
}
