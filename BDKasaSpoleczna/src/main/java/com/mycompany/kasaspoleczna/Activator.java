package com.mycompany.kasaspoleczna;

import com.mycompany.bazakart.Baza;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private KasaSpoleczna kasa;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.kasa=new KasaSpoleczna();
        this.context=context;
        context.registerService(Baza.class.getName(), this.kasa, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
