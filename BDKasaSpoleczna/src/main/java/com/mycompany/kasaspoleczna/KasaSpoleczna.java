/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.kasaspoleczna;

import com.mycompany.bazakart.*;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class KasaSpoleczna extends Baza{
    public KasaSpoleczna(){
        super();
        this.ikona=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/kasaspoleczna/grafika/kasaspoleczna.png")).getImage();
        this.nazwa="KASA SPOŁECZNA";
        

        this.karty[0].setOpis("<html> Zapłać rachunek za szpital 100 zł </html>");
        this.karty[0].setKwota(-100);
        this.karty[0].setRuch(50);
        this.karty[1].setOpis("<html> Honorarium lekarza. </br> Zapłać 50 zł </html>");
        this.karty[1].setKwota(-50);
        this.karty[1].setRuch(50);
        this.karty[2].setOpis("<html>Idź do więzienia. </br> Przejdź prosto do więzienia! </br> Nie przechodź przez START </html>");
        this.karty[2].setKwota(0);
        this.karty[2].setRuch(30);
        this.karty[3].setOpis("<html> Zapłać składkę ubezpieczeniową 50zł<html>");
        this.karty[3].setKwota(-50);
        this.karty[3].setRuch(50);
        this.karty[4].setOpis("Przejdź na START");
        this.karty[4].setKwota(0);
        this.karty[4].setRuch(0);
        this.karty[5].setOpis("<html> Sprzedałeś obligacje. </br> Pobierz 200zł</html> ");
        this.karty[5].setKwota(200);
        this.karty[5].setRuch(50);
        this.karty[6].setOpis("<html> Dziś są Twoje urodziny. </br> Pobierz po 10zł od każdego gracza </html>");
        this.karty[6].setKwota(50);
        this.karty[6].setRuch(50);
        this.karty[7].setOpis("<html>Wyjdź bezpłatnie z więzienia. Tę kartę możesz </br> zatrzymać do późniejszego wykorzystania lub sprzedaży.</html>");
        this.karty[7].setKwota(666);
        this.karty[7].setRuch(50);
        this.karty[8].setOpis("<html>Wróć na Ulicę Konopacką </html>");
        this.karty[8].setKwota(0);
        this.karty[8].setRuch(1);
        this.karty[9].setOpis("Odziedziczyłeś w spadku 100zł");
        this.karty[9].setKwota(100);
        this.karty[9].setRuch(50);
        this.karty[10].setOpis("<html>Otrzymujesz odsetki od lokaty terminowej. </br> Pobierz 25zł</html>");
        this.karty[10].setKwota(25);
        this.karty[10].setRuch(50);
        this.karty[11].setOpis("Otrzymujesz 50zł za sprzedane akcje");
        this.karty[11].setKwota(50);
        this.karty[11].setRuch(50);
        this.karty[12].setOpis("<html>Otrzymujesz zwrot podatku dochodowego. </br> Pobierz 20zł </html>");
        this.karty[12].setKwota(20);
        this.karty[12].setRuch(50);
        this.karty[13].setOpis("<html>Wygrana druga nagroda w konkursie piękności. </br> Pobierz 10 zł<html>");
        this.karty[13].setKwota(10);
        this.karty[13].setRuch(50);
        this.karty[14].setOpis("<html>Błąd bankowy na Twoją korzyść. </br> Pobierz 200zł<html>");
        this.karty[14].setKwota(200);
        this.karty[14].setRuch(50);
        this.karty[15].setOpis("Zapłać 10zł grzywny");
        this.karty[15].setKwota(-10);
        this.karty[15].setRuch(50);
        
        //jak ruch jest >40 nigdzie się nie ruszamy!
    }
    @Override
    public int getImmunitet() {
        if(wybrana.getKwota()!=666)
           return 0;
        if(wybrana.getRuch()==30)
           return -1;
        return 1;
    }
}
