/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gra;

import com.mycompany.about.About;
import com.mycompany.about.contract.IPobierzInfo;
import com.mycompany.bank.Bank;
import com.mycompany.bank.contracts.IBank;
import com.mycompany.bazakart.Baza;
import com.mycompany.bazakart.contracts.IBaza;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.Gracz;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.gracz.contracts.IWiezien;
import com.mycompany.instrukcja.Instrukcja;
import com.mycompany.instrukcja.contracts.IPomoc;
import com.mycompany.mapa.Mapa;
import com.mycompany.mapa.contract.IMapa;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pionek.contracts.IPionek;
import com.mycompany.plansza.Plansza;
import java.awt.AWTException;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author Joanna667
 */
@org.apache.felix.scr.annotations.Component(name = "Gra",
        immediate = true, enabled = true, metatype = true)
@Service
public class Gra extends javax.swing.JFrame implements BundleActivator, Runnable, KeyListener, MouseListener{

    /**
     * Creates new form Gra
     */
    private List<IGracz> listaGraczy;
    private List<IPionek> listaPionkow;
    private IGracz gracz;
    private Plansza plansza=null;
    private BundleContext context;
    private ServiceReference[] listaMap;
    private ServiceReference[] dostepneKonta;
    private ServiceReference to;
    private static JFrame okienko = null;
    private IMapa m;
    private IBaza karty;
    private IBank bank;
    private IPobierzInfo informacja;
    private IPomoc pomoc;
    private List<String> info;
    //servicetrackery
    private ServiceTracker gracze;
    private ServiceTracker mapy;
    private String komunikat;
    private int ruch;
    private JList<INieruchomosc> dzialkiBanku;
    private JList<INieruchomosc> dzialkiGracza;
    private int pozycja1;
    public Gra() {
        initComponents();
             //instancja klasy pionek jest tworzona w konstruktorze klasy Gracz tutaj tylko przydzielamy im ikonki
            //trzeba dodać service trackera i graczy z sieci, gracze 1,2,3,4,5 są stałą listą
            //to, którzy gracze są aktywni zależy od liczby chętnych w sieci
            this.context=context;
            this.ruch=0;
            this.dzialkiBanku=new JList<INieruchomosc>(){};
            dzialkiBanku.setLocation(0, 0);
            dzialkiBanku.setSize(200, 400);
            dzialkiBanku.setVisible(true);
            jPanel7.add(dzialkiBanku);
            this.dzialkiGracza=new JList<INieruchomosc>(){};
            dzialkiGracza.setLocation(0, 0);
            dzialkiGracza.setSize(200, 300);
            dzialkiGracza.setVisible(true);
            jPanel4.add(dzialkiGracza);
            System.out.println("Otwieranie gry...");

    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jButton1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel5.setText("jLabel5");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 324, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 407, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 246, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 325, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 179, Short.MAX_VALUE)
        );

        jButton2.setText("O PROJEKCIE");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(0, 153, 0));
        jButton3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton3.setText("GRAJ");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 767, Short.MAX_VALUE)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jButton1.setFont(new java.awt.Font("Trebuchet MS", 0, 10)); // NOI18N
        jButton1.setText("Zasady gry");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel4.setText("jLabel4");

        jButton6.setBackground(new java.awt.Color(102, 204, 0));
        jButton6.setText("Rusz się!");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel3.setText("jLabel3");

        jLabel6.setText("jLabel6");

        jButton7.setBackground(new java.awt.Color(0, 153, 0));
        jButton7.setText("Ruch specjalny");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(87, 161, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLayeredPane1)))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
         informacja.show();
    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
            if(this.gracze.getServiceReferences().length==0)
            {
                JOptionPane.showMessageDialog(this, "Brak dostępnych kont!");
            }
            else{
                
            this.dostepneKonta=new ServiceReference[this.gracze.getServiceReferences().length];
            this.dostepneKonta=this.gracze.getServiceReferences();
            IPionek[] pionki = new IPionek[dostepneKonta.length];
            for (int i = 0; i < dostepneKonta.length; i++) {
                IGracz g = (IGracz) context.getService(dostepneKonta[i]);
                if (g!=null) {
                    g.getPionek().setPozycja(0);
                    plansza.usunPionek(g.getPionek().getNumer()-1);
                    pionki[g.getPionek().getNumer()-1]=g.getPionek();
                }
            }
            
            plansza.setPionki(pionki);
            this.gracz=(Gracz)context.getService(dostepneKonta[0]);
            this.plansza.pokazKomunikat("Grę rozpoczyna: "+gracz.getImie());
            }

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       // this.usunGracza(0);
        ServiceReference[] help;
        try {
            help = context.getServiceReferences(Instrukcja.class.getName(), null);
                        if (help.length > 0) {
                IPomoc p = (IPomoc) context.getService(help[0]);
                p.setLista(info);
                this.pomoc=p;  
                pomoc.pokaz();
            }
        } catch (InvalidSyntaxException ex) {
            Logger.getLogger(Gra.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButton1ActionPerformed
    //rusz się!
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed

        if(sprawdzGracza()){
           this.pozycja1=gracz.getPionek().getPozycja();
           this.plansza.ruszPionkiem(gracz.getPionek().getNumer()-1);
           plansza.pokazKomunikat(obsluzAkcje(gracz));
        }
        else{ 
            nastepnyProsze();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        this.pozycja1=gracz.getPionek().getPozycja();
        obsluzSzanse();
        obsluzAkcje(gracz);
    }//GEN-LAST:event_jButton7ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    // End of variables declaration//GEN-END:variables

    private void DodajUsługi(){
      dzialkiGracza.addMouseListener( new MouseAdapter()
     {
        @Override
        public void mousePressed(MouseEvent e)
        {
            JPopupMenu menu = new JPopupMenu();
                JMenuItem item = new JMenuItem("Sprzedaj dzialke");
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                       INieruchomosc n = (INieruchomosc) dzialkiGracza.getSelectedValue();
                       int i = n.getId();
                       if(n.getLevel()==0)
                         bank.zwrotNieruchomosci(n);
                       else 
                         plansza.pokazKomunikat("Sprzedać można tylko pustą działkę");
                    }
                });
                menu.add(item);
                 JMenuItem item1 = new JMenuItem("Postaw dom");
                item1.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                    INieruchomosc n = (INieruchomosc) dzialkiGracza.getSelectedValue();
                    int i = n.getId();
                    n.setLevel(n.getLevel()+1);
                    try{
                    if(bank.zezwolenieNaBudowe(gracz, n))
                        plansza.postawDom((IDzialka)n);
                    else
                      komunikat="Żeby postawić dom należy posiadać całą dzielnicę";
                    }
                    catch(Exception ex){
                        System.out.print(ex);
                        komunikat = "To nie jest działka budowlana!";
                        JOptionPane.showMessageDialog(dzialkiGracza, komunikat);
                    }
                    }
                });
                menu.add(item1);
                 JMenuItem item2 = new JMenuItem("Sprzedaj dom");
                item2.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                    INieruchomosc n = (INieruchomosc) dzialkiGracza.getSelectedValue();
                    int i = n.getId();
                    n.setLevel(n.getLevel()-1);
                    try{
                    plansza.sprzedajDom((IDzialka) n);
                    }
                    catch(Exception ex){
                        komunikat = "Działka jest pusta!";
                        JOptionPane.showMessageDialog(dzialkiGracza, komunikat);  
                    }
                    }
                });
                menu.add(item2);
                 JMenuItem item3 = new JMenuItem("Pobierz czynsz");
                item3.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                      INieruchomosc n = (INieruchomosc) dzialkiGracza.getSelectedValue();
                      if(n!=null){
                      int i = n.getId();
                      //List<IGracz> gracze = new ArrayList<IGracz>(){};
                        for (int j = 0; j < listaGraczy.size(); j++) {
                            if (listaPionkow.get(i).getPozycja()==i && !(listaGraczy.get(i).getImie().equals(gracz.getImie()))) {
                                int s = listaGraczy.get(i).getSrodki();
                                listaGraczy.get(i).setSrodki(s-n.getCzynsz());
                                
                            }
                        }
                      }
                    }
                });
                menu.add(item3);
                menu.show(dzialkiGracza, 5, dzialkiGracza.getCellBounds(
                        dzialkiGracza.getSelectedIndex(),
                        dzialkiGracza.getSelectedIndex()).y);
        }
     });
       dzialkiBanku.addMouseListener(new MouseAdapter()
     {
      @Override
      public void mousePressed(MouseEvent e)
         {
                JPopupMenu menu = new JPopupMenu();
                JMenuItem item = new JMenuItem("Kup działkę");
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e){
                     INieruchomosc n = (INieruchomosc) dzialkiBanku.getSelectedValue();
                     if(n!=null &&n.getWlasciciel().equals("bank")){
                    int i = n.getId();
                    jLabel5.setText(gracz.getImie() + "-stan konta");
                    bank.kupNieruchomosc(gracz, i);
                    jLabel4.setText(gracz.getSrodki()+"zł");
                    dzialkiBanku.repaint();
                     }
                     else JOptionPane.showMessageDialog(null, "Działka niedostępna!");

                    }

                }
                );
                menu.add(item);
                menu.show(dzialkiBanku, 5, dzialkiBanku.getCellBounds(
                        dzialkiBanku.getSelectedIndex(),
                        dzialkiBanku.getSelectedIndex()).y);
            }
     });
    }
    public void run() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setSize(800, 1200);
            this.listaGraczy=new ArrayList<IGracz>(){};
            jLayeredPane1.setLayout(new GridLayout(1,1));
            this.info=new ArrayList<String>(){};
            info.add("Gra Monopoly");
            info.add("ZASADY GRY: ");
            info.add("1. ");
            info.add("2. ");
            jLabel6.setText("Wszystkie nieruchomości: ");
            jLabel5.setText("Mój stan konta: ");
            jLabel3.setText("Moje nieruchomości:");
            jLabel4.setText("");
            this.DodajUsługi();
            this.mapy=new ServiceTracker(context, Mapa.class.getName(),null);
            this.gracze=new PlayerServiceTracker(context, Gracz.class.getName(),jPanel5,buttonGroup5);
            this.komunikat="   ";


        this.setSize(800, 1200);
        this.setVisible(true);
           this.mapy.open();
           this.gracze.open();
    }
    //zależne od wartości zwracanej przez metody dotyczące ruchu pionkow klasy Plansza
    //(pozycji na jaką ustawił się pionek danego gracza
    public String obsluzAkcje(IGracz gracz) {
        int i = gracz.getPionek().getPozycja();
        
        IGracz g = gracz;
        switch(i){
            case 0: {
            m.getStart().PobierzPensje(gracz);
              if(plansza.getRuch())
                    return "Gracz "+g.getImie()+" pobrał pensję, teraz wykonuje kolejny ruch";
              else return "Gracz "+g.getImie()+" pobrał pensję, teraz ruch wykonuje "+this.nastepnyProsze();

            }
            case 7: case 22: case 36:{
                this.karty=m.getSzansa();
              return "Wybierz kartę z opcji SZANSA";
              
            }
            case 2: case 17: case 33:{
                this.karty=m.getKasaSpoleczna();
              return "Wybierz kartę z opcji KASA SPOŁECZNA";
            }
            case 30:{
               boolean b = m.getWiezienie().Aresztuj(gracz);
               plansza.wiezienie(gracz.getPionek().getNumer()-1);
               if(b==true)
                  return "Gracz "+g.getImie()+" jest teraz w więzieniu, teraz ruch wykonuje "+nastepnyProsze();
               else if(plansza.getRuch()) return "Gracz "+g.getImie()+" uniknął kary, teraz wykonuje kolejny ruch";
               else return "Gracz "+g.getImie()+" uniknął kary, teraz ruch wykonuje "+nastepnyProsze();
            }
            case 10:{
                if(m.getWiezienie().getWiezniowie().contains(g)){
                   return "Gracz "+g.getImie()+" jest teraz w więzieniu, teraz ruch wykonuje "+nastepnyProsze(); }
                else if(plansza.getRuch())
                   return "Gracz "+g.getImie()+" odwiedził więzienie, teraz wykonuje kolejny ruch";
                else return "Gracz "+g.getImie()+" odwiedził więzienie, teraz ruch wykonuje "+nastepnyProsze(); 
            }
            case 4:{
                m.getPodatki()[0].zaplac(gracz);
                if(plansza.getRuch())
                   return "Gracz "+g.getImie()+" zapłacił "+m.getPodatki()[0].getOpis()+ "teraz wykonuje kolejny ruch";
                else return "Gracz "+g.getImie()+" zapłacił "+m.getPodatki()[0].getOpis()+ " teraz ruch wykonuje "+nastepnyProsze();
            }
            case 38:{
                m.getPodatki()[1].zaplac(gracz);
                if(plansza.getRuch())
                   return "Gracz "+g.getImie()+" zapłacił "+m.getPodatki()[1].getOpis()+ "teraz wykonuje kolejny ruch";
                else return "Gracz "+g.getImie()+" zapłacił "+m.getPodatki()[1].getOpis()+" teraz ruch wykonuje "+nastepnyProsze();
            }
            default:{
                if(i<pozycja1){ m.getStart().PobierzPensje(gracz);
                       JOptionPane.showMessageDialog(this, "Gracz "+g.getImie()+" pobrał pensję");
                        }
                if(!plansza.getRuch())
                    return "Gracz "+g.getImie()+" poszedł na "+m.getPola()[gracz.getPionek().getPozycja()].getNazwa()+" teraz ruch wykonuje "+nastepnyProsze(); 
                else return "Gracz "+g.getImie()+" poszedł na "+m.getPola()[gracz.getPionek().getPozycja()].getNazwa()+" teraz wykonuje kolejny ruch"; 
            }
            
            
            
        }
    }

    public void obsluzSzanse(){
       int r = karty.getRuch();
       int i = karty.getImmunitet();
       int k=karty.getKwota();
       int srodki = gracz.getSrodki();
       int imm = gracz.getImmunitet();
       //kwota może być ujemna lub dodatnia
       gracz.setSrodki(srodki+k);
       if(i<0)
           gracz.setImmunitet(imm-i);
       else
          gracz.setImmunitet(imm+i);
       if(gracz.getImmunitet()>=0)
       {
       if (pozycja1>r && r>=0) {
           m.getStart().PobierzPensje(gracz);
        }
       plansza.ustawPionek(r, gracz.getPionek().getNumer()-1);
       }
       else{
           m.getWiezienie().Aresztuj(gracz);
           plansza.wiezienie(gracz.getPionek().getNumer()-1);
       }
    }
    public String nastepnyProsze(){
           if (ruch<dostepneKonta.length-1) {
                        this.ruch+=1;
                        if(((Gracz)context.getService(dostepneKonta[ruch])).sprawdz()==true)
                            this.gracz=(Gracz)context.getService(dostepneKonta[ruch]); 
                        else{
                            ruch+=1;
                            this.gracz=(Gracz)context.getService(dostepneKonta[ruch]); 
                        }
              
                        
                    }
                    else {
                      
                        ruch=0;
                        this.gracz=(Gracz)context.getService(dostepneKonta[ruch]);
                         // plansza.pokazKomunikat("Koniec kolejki, rozpoczyna gracz: "+gracz.getImie());
                    } 
            jLabel5.setText(gracz.getImie()+" - stan konta:");
             jLabel4.setText(gracz.getSrodki()+"zł");
             dzialkiGracza.setListData(gracz.getObiekty().toArray(new INieruchomosc[gracz.getObiekty().size()]));
             dzialkiBanku.setListData(bank.getOferta().toArray(new INieruchomosc[bank.getOferta().size()]));
             dzialkiGracza.repaint();
             dzialkiBanku.repaint();
             this.plansza.setRuch(false);
           return this.gracz.getImie();
    }
    public void dodajGracza(String imie) {
      
        for (int i = 0; i < this.listaGraczy.size(); i++) {
            IGracz g = (Gracz) listaGraczy.get(i);
                g.setImie(imie);
               listaGraczy.set(i, g);    
            }

   }
    public void usunGracza(int i){
        this.gracze.removedService(dostepneKonta[i], gracz);
        this.plansza.usunPionek(i);
    }
    public void transakcja(int i){
        
    }
    @Activate
    public void start(BundleContext bc) throws Exception {
 
 
        
        //context.registerService(Gra.class.getName(), this, null);
       try{

            this.context=bc;
            ServiceReference[] b = context.getServiceReferences(Bank.class.getName(), null);
            ServiceReference[] plansze = context.getServiceReferences(Plansza.class.getName(), null);
            ServiceReference[] mapServices=context.getServiceReferences(Mapa.class.getName(), null);
            ServiceReference[] accounts=context.getServiceReferences(Gracz.class.getName(), null);
            ServiceReference[] sz = context.getServiceReferences(Baza.class.getName(), null);
            ServiceReference[] about = context.getServiceReferences(About.class.getName(), null);
            if (b.length > 0) {
                Bank b1=(Bank) context.getService(b[0]);
                this.bank = b1;
            }
            
           if (plansze.length > 0){
                Plansza p1 = (Plansza)context.getService(plansze[0]);
                this.plansza = p1;
           }
          
            for (ServiceReference ms : mapServices) {
                IMapa mapa = (IMapa) context.getService(ms);
                if (mapa!=null) {
                    if (sz.length > 0) {
                        IBaza szansa = (IBaza) context.getService(sz[0]);
                        mapa.setSzansa(szansa);
                        IBaza kasaspol= (IBaza) context.getService(sz[1]);
                        mapa.setKasaSpoleczna(kasaspol);
                        this.m=mapa;
                        // ładuję mapkę
                        
//                        List<Dzielnica> dzialki = m.getGrupy();
//                        INieruchomosc[][] obiekty =(INieruchomosc[][]) m.getGrupyObiektow();
                        this.bank.pobierzMape(m);
//                        this.bank.setDzialki(dzialki.toArray(new Dzielnica[dzialki.size()]));
//                        this.bank.setObiekty(obiekty);
//                        this.bank.aktualizujOferte();
                        dzialkiBanku.setListData(bank.getOferta().toArray(new INieruchomosc[bank.getOferta().size()]));
                        dzialkiGracza.setListData(new INieruchomosc[1]);
                        this.plansza.setMapa(m);
                        break;
                    }
                }
            
            }
            if (about.length > 0) {
                this.informacja = (IPobierzInfo) context.getService(about[0]);
                this.informacja.setVersion("1.0.0.0");
                this.informacja.setAuthors("Joanna Sosnowska", "nikt", "nikt");
            }

                   } catch (Exception ex) {

        }
       jLayeredPane1.add(plansza);
        //to = context.registerService(Gra.class.getName(), this, null).getReference();
        run();
    }
    @Deactivate
    public void stop(BundleContext bc) throws Exception {
          if (mapy != null) {
            this.mapy.close();
        }
        if (gracze != null) {
            this.gracze.close();
        }
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                setVisible(false);
                dispose();
            }
        });
    }
    public boolean sprawdzGracza(){
        if (m.getWiezienie().getWiezniowie().contains(gracz)) {
                IWiezien w = (IWiezien)gracz;
               w.setKara(w.getKara()-1);
               if((w.getKara()==0)){
                   m.getWiezienie().Uwolnij(gracz);
                    plansza.ustawPionek(10, gracz.getPionek().getNumer()-1); 
                            plansza.pokazKomunikat(gracz.getImie()+" wyszedł z więzienia");
                            return true;
               }     
               plansza.pokazKomunikat(gracz.getImie()+" siedzi w więzieniu, do końca: "+w.getKara()+"rundy/a");
               return false;
            }
            else if(gracz.sprawdz()==false){
                bank.konfiskujMajatek(gracz);
              gracze.remove((ServiceReference)gracz);
               plansza.pokazKomunikat(gracz.getImie()+" jest BANKRUTEM, kończy grę");
               usunGracza(ruch);
               return false;

            }
            else{ 
                plansza.pokazKomunikat(gracz.getImie()+" - wykonał ruch");
                return true;
            }
    }
    public void keyTyped(KeyEvent e) {
        
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key==KeyEvent.VK_SPACE) {
   
                
        }

    }

    public void keyReleased(KeyEvent e) {
        
    }

    public void mouseClicked(MouseEvent e) {
        //plansza.ruszPionkiem(gracz.getPionek().getNumer()-1);
    }

    public void mousePressed(MouseEvent e) {
       
    }

    public void mouseReleased(MouseEvent e) {
        
    }

    public void mouseEntered(MouseEvent e) {
       
    }

    public void mouseExited(MouseEvent e) {
       
    }
    
    
    //
    private class PlayerServiceTracker extends ServiceTracker {

        JPanel panel;
        ButtonGroup bgroup;
        public PlayerServiceTracker(BundleContext context, String klasa, JPanel panel, ButtonGroup bg) {
            super(context, klasa, null);
            this.panel = panel;
            this.bgroup = bg;
        }

        @Override
        public Object addingService(ServiceReference reference) {
          Object obj = (Object) context.getService(reference);
            IGracz g = (IGracz) obj;

            Image logo = g.getPionek().getIkonka();

            if (logo != null) {
                JRadioButton button = new JRadioButton();
                button.setOpaque(false);
                if (this.bgroup.getSelection()==null) {
                   button.setSelected(true);
                }
                button.setActionCommand(g.getImie());
                button.setIcon(new ImageIcon(logo));
                button.setForeground(java.awt.Color.darkGray);
                button.setVisible(true);
                this.panel.add(button);
                this.bgroup.add(button);
            } else {
                JRadioButton button = new JRadioButton();
                button.setOpaque(false);
                button.setActionCommand(g.getImie());
                button.setIcon(new ImageIcon(logo));
                button.setForeground(java.awt.Color.darkGray);
                button.setVisible(true);
                this.panel.add(button);
                this.bgroup.add(button); 
                
            }

            this.panel.validate();
            this.panel.repaint();

            return obj; 
        }

        @Override
        public void modifiedService(ServiceReference sr, Object o) {

        }

        @Override
        public void removedService(ServiceReference reference, Object service) {
            IGracz gr = (IGracz) service;
            String nazwaGracza = gr.getImie();
            for (Component c : this.panel.getComponents()) {
                if (!(c instanceof JRadioButton)) {
                    continue;
                }

                JRadioButton button = (JRadioButton) c;
                if (!(nazwaGracza.contentEquals(button.getText()))) {
                    continue;
                }

                this.panel.remove(button);
                this.bgroup.remove(button);
                break;
            }
            this.panel.revalidate();
            this.panel.repaint();

            context.ungetService(reference);
        }

    }
}
