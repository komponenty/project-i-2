/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.obiekt.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.pole.contract.IPole;

/**
 *
 * @author st
 */
public interface INieruchomosc extends IPole {
    int getCena();
    void zakup(IGracz g);
    void sprzedaj(IGracz g);
    void oddaj();
    void setCzynsz(int level);
    int getCzynsz();
    int getHipoteka();
    void setTyp(String s);
    int getLevel();
    void setLevel(int i);
    String getWlasciciel();
    void oddajwzastaw();
    void wykupzastaw();
    boolean getStatus();
    
}
