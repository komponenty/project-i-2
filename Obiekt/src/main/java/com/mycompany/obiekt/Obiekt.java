/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.obiekt;

import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pionek.contracts.IPionek;
import com.mycompany.pole.Pole;
import java.awt.Image;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author Joanna
 */
public class Obiekt extends Pole implements INieruchomosc {
    protected String typ;
    protected int level;
    protected int cena;
    protected boolean zastawiona;
    protected String wlasciciel;
    protected int czynszpierwotny;
    protected int czynsz;
    protected int hipoteka;
    protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Obiekt(String typ, Image ikona,int cena, String tytul){
        super();
        this.ikona=ikona;
        this.zastawiona=false;
        this.wlasciciel="bank";
        this.cena=cena;
        this.tytul=typ+ " "+ tytul;
        this.hipoteka=cena/2;
    }
    public Obiekt(int cena, String tytul){
        super();

        this.zastawiona=false;
        this.wlasciciel="bank";
        this.cena=cena;
        this.tytul=tytul;
        this.hipoteka=cena/2;
    }
    
    public int getCena() {
        return cena;
    }

    public void setCena(int c) {
        this.cena=c;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int l) {
        this.level=l;
    }
    
    public void setNazwa(String nazwa) {
        this.tytul=nazwa;
    }

    public void zakup(IGracz g) {
        this.wlasciciel=g.getImie();
        
           
    }

    public void oddaj() {
        this.level=0;
        this.wlasciciel=null;
        
    }
    //s to może być cokolwiek level lub liczba wyrzuconych oczek
    public void setCzynsz(int s) {
        if (typ=="Dworzec") {
            this.czynsz=25*(s+1);
        }
        else if(level==1){
            this.czynsz=4*s;
        }
        else{
            this.czynsz=10*s;
        }
       
    }

    public int getCzynsz() {
        return czynsz;
    }

    public int getHipoteka() {
        return hipoteka;
    }

    public void setTyp(String t) {
        this.typ=t;
    }

    public String getWlasciciel() {
        return wlasciciel;
    }

    public void sprzedaj(IGracz g) {
        this.wlasciciel=g.getImie();
    }

    public boolean getStatus() {
       return zastawiona;
    }

    public void oddajwzastaw() {
        this.zastawiona=true;
    }

    public void wykupzastaw() {
        this.zastawiona=false;
    }

    
    
}
