/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.bazakart;

import com.mycompany.bazakart.contracts.IBaza;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.karta.Karta;
import com.mycompany.karta.contract.*;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Joanna667
 */
public class Baza extends JPanel implements IBaza, MouseListener {
    protected IKarta[] karty;
    protected String nazwa;
    protected Image ikona;
    protected JLabel etykietka;
    protected IKarta wybrana;
    public Baza(){
       this.setLayout(new GridLayout(1,1));
       if(ikona!=null)
           this.etykietka = new JLabel(nazwa, new ImageIcon(ikona), JLabel.CENTER);
        else{
           this.etykietka = new JLabel();
           etykietka.setText(nazwa);
        }
        this.karty=new IKarta[16];
        for (int i = 0; i < karty.length; i++) {
            karty[i]=new Karta("", 0,0);
        }
        this.add(etykietka);
        this.wybrana=new Karta("",0,0);
    }
    public IKarta[] getObiekty() {
        return karty;
    }

    public void setObiekty(IKarta[] zestaw) {
        this.karty=zestaw;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa=nazwa;
    }

    public IKarta Losuj() {
       
       Random r = new Random();
       int i = r.nextInt(karty.length);
       IKarta k = karty[i];
       this.etykietka.setText(k.getOpis());
       this.wybrana=k;
       return wybrana;
       
    }
    public void mouseClicked(MouseEvent e) {
        this.Losuj();
    }

    public void mousePressed(MouseEvent e) {
        
    }

    public void mouseReleased(MouseEvent e) {
        
    }

    public void mouseEntered(MouseEvent e) {
        
    }

    public void mouseExited(MouseEvent e) {
        
    }

    public Image pokaz() {
        return ikona;
    }
    public int getKwota() {
      return wybrana.getKwota();
    }
    public int getRuch() {
        return wybrana.getRuch();
       
    }

    public int getImmunitet() {
        return 0;
    }
    
}
