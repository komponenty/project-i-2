/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.bazakart.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.karta.contract.IKarta;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author Joanna667
 */
public interface IBaza {
    IKarta[] getObiekty();
    void setObiekty(IKarta[] zestaw);
    String getNazwa();
    void setNazwa(String nazwa);
    int getRuch();
    IKarta Losuj();
    Image pokaz();
    int getKwota();
    int getImmunitet();
    
//    void dopasuj(Dimension d);
}
