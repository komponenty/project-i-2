package com.mycompany.plansza;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    BundleContext context;
    Plansza plansza;
    public void start(BundleContext context) throws Exception {
       this.plansza=new Plansza();
       this.context=context;
       context.registerService(Plansza.class.getName(), this.plansza, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
