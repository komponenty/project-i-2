/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.plansza.contracts;

import com.mycompany.mapa.contract.IMapa;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.pionek.contracts.IPionek;
import javax.swing.JButton;

/**
 *
 * @author Joanna667
 */
public interface IPlansza {
    void setMapa(IMapa mapa);
    void setPionki(IPionek[] pionki);
    void usunPionek(int id);
    boolean getRuch();
    void setRuch(boolean b);
    boolean ustawPionek(int i, int idpionka);
    void pokazKomunikat(String s);
    int ruszPionkiem(int i);
    void postawDom(IDzialka d);
    void sprzedajDom(IDzialka d);
    void wiezienie(int idpionka);
}
