/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.budynek;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;

/**
 *
 * @author Joanna667
 */
public class Budynek extends JButton {
    private String typ;
    public Budynek(String rodzaj){
        super();
        this.typ=rodzaj;
        this.setOpaque(true);
        if ("dom".equals(typ) || "Dom".equals(typ)) {
            this.setBackground(Color.green);
            this.setPreferredSize(new Dimension(10,10));
        }
        else{
            this.setBackground(Color.red);
            this.setPreferredSize(new Dimension(10,20));  
        }
        this.setVisible(true);
    }
    @Override
    public void setLocation(int x, int y){
        super.setLocation(x, y);
    }
}
