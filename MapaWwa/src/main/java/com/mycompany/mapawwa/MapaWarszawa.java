/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.mapawwa;

import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.idzdowiezienia.IdzDoWiezienia;
import com.mycompany.mapa.Mapa;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.obiekt.Obiekt;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.parking.Parking;
import com.mycompany.podatek.Podatek;
import com.mycompany.start.Start;
import com.mycompany.szansa.Szansa;
import com.mycompany.wiezienie.Wiezienie;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import org.osgi.framework.InvalidSyntaxException;

/**
 *
 * @author Joanna667
 */
public class MapaWarszawa extends Mapa {
    
    private final Szansa poleszansa;
    private final Szansa kasaspoleczna;
    private Obiekt[] dworce;
    private Obiekt[] obiekty1;
    
    public MapaWarszawa(String title) throws InvalidSyntaxException {
        super(title);
            this.logo=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/logo.png")).getImage();
            this.tablicaGrup=new ArrayList<Dzielnica>(){};
            this.dworce=new Obiekt[4];
            this.obiekty1=new Obiekt[2];
            this.grupyObiektow=new INieruchomosc[2][];
            Image szansaicon = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/szansa_mini.png")).getImage();
            Image kasaicon = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/kasaspoleczna_mini.png")).getImage();

            Image elka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/elektrownia.png")).getImage();
            Image wodociagi = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/wodociagi.png")).getImage();
            Image podatek = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/forsa.png")).getImage();
            Image dworzec = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/mapawwa/grafika/dworzec.png")).getImage();
      
        this.start=new Start();
        pola[0]=start;
        this.wiezienie=new Wiezienie();
        pola[10]=wiezienie;
        this.parking=new Parking();
        pola[20]=parking;
        this.idzdowiezienia=new IdzDoWiezienia();
        pola[30]=idzdowiezienia;
        this.kasaspoleczna=new Szansa(kasaicon, "Kasa Społeczna", this.kasaspol);
        kasaspoleczna.setOpis("KASA SPOŁECZNA");
        pola[2]=kasaspoleczna;
        pola[17]=kasaspoleczna;
        pola[33]=kasaspoleczna;
        this.poleszansa=new Szansa(szansaicon,"SZANSA", this.szansa);
        poleszansa.setOpis("SZANSA");
        pola[7]=poleszansa;
        pola[22]=poleszansa;
        pola[36]=poleszansa;
        Nieruchomosc nieruchomosc1=new Nieruchomosc(60,"Ulica Konopacka",50,Color.BLACK);
        nieruchomosc1.setId(1);
        nieruchomosc1.setCzynsz(2);
        listaNieruchomosci[0]=nieruchomosc1;
        pola[1]=nieruchomosc1;
        Nieruchomosc nieruchomosc2=new Nieruchomosc(60,"Ulica Stalowa",50,Color.BLACK);
        listaNieruchomosci[1]=nieruchomosc2;
        nieruchomosc2.setId(3);
        nieruchomosc2.setCzynsz(4);
        pola[3]=nieruchomosc2;
        Podatek podatek1=new Podatek(podatek, 200);
        podatek1.setOpis("podatek dochodowy");
        podatki[0]=podatek1;
        pola[4]=podatek1;
        Obiekt obiekt1=new Obiekt("Dworzec", dworzec, 200, "Zachodni");
        listaObiektow[0]=obiekt1;
        dworce[0]=obiekt1;
        obiekt1.setId(5);
        pola[5]=obiekt1;
        Nieruchomosc nieruchomosc3=new Nieruchomosc(100,"Ulica Radzymińska",50,Color.blue);
        nieruchomosc3.setId(6);
        nieruchomosc3.setCzynsz(6);
        listaNieruchomosci[2]=nieruchomosc3;
        pola[6]=nieruchomosc3;
        Nieruchomosc nieruchomosc4=new Nieruchomosc(100,"Ulica Jagiellońska",50,Color.BLUE);
        nieruchomosc4.setCzynsz(6);
        nieruchomosc4.setId(8);
        listaNieruchomosci[3]=nieruchomosc4;
        pola[8]=nieruchomosc4;
        Nieruchomosc nieruchomosc5=new Nieruchomosc(120,"Ulica Targowa",50,Color.BLUE);
        nieruchomosc5.setCzynsz(8);
        nieruchomosc5.setId(9);
        listaNieruchomosci[4]=nieruchomosc5;
        pola[9]=nieruchomosc5;
        Nieruchomosc nieruchomosc6=new Nieruchomosc(140,"Ulica Płowiecka",100,Color.PINK);
        listaNieruchomosci[5]=nieruchomosc6;
        nieruchomosc6.setCzynsz(10);
        nieruchomosc6.setId(11);
        pola[11]=nieruchomosc6;
        Obiekt obiekt2=new Obiekt("",elka,200,"Elektrownia");
        obiekty1[0]=obiekt2;
        obiekt2.setId(12);
        listaObiektow[1]=obiekt2;
        pola[12]=obiekt2;
        Nieruchomosc nieruchomosc7=new Nieruchomosc(140,"Ulica Grochowska",100,Color.PINK);
        listaNieruchomosci[6]=nieruchomosc7;
        nieruchomosc7.setCzynsz(12);
        nieruchomosc7.setId(13);
        pola[13]=nieruchomosc7;
        Nieruchomosc nieruchomosc8=new Nieruchomosc(160,"Ulica Marsa",100,Color.PINK);
        nieruchomosc8.setCzynsz(10);
        nieruchomosc8.setId(14);
        listaNieruchomosci[7]=nieruchomosc8;
        pola[14]=nieruchomosc8;
        Obiekt obiekt3=new Obiekt("Dworzec", dworzec, 200, "Gdański");
        dworce[1]=obiekt3;
        obiekt3.setId(15);
        listaObiektow[2]=obiekt3;
        pola[15]=obiekt3;
        Nieruchomosc nieruchomosc9=new Nieruchomosc(180,"Ulica Obozowa",100,Color.ORANGE);
        nieruchomosc9.setCzynsz(14);
        nieruchomosc9.setId(16);
        listaNieruchomosci[8]=nieruchomosc9;
        pola[16]=nieruchomosc9;
        Nieruchomosc nieruchomosc10=new Nieruchomosc(180,"Ulica Górczewska",100,Color.ORANGE);
        nieruchomosc10.setCzynsz(14);
        nieruchomosc10.setId(17);
        listaNieruchomosci[9]=nieruchomosc10;
        pola[18]=nieruchomosc10;
        Nieruchomosc nieruchomosc11=new Nieruchomosc(200,"Ulica Wolska",100,Color.ORANGE);
        nieruchomosc11.setCzynsz(16);
        nieruchomosc11.setId(19);
        listaNieruchomosci[10]=nieruchomosc11;
        pola[19]=nieruchomosc11;
        Nieruchomosc nieruchomosc12=new Nieruchomosc(220,"Ulica Mickiewicza",150,Color.RED);
        nieruchomosc12.setCzynsz(18);
        nieruchomosc12.setId(21);
        listaNieruchomosci[11]=nieruchomosc12;
        pola[21]=nieruchomosc12;
        Nieruchomosc nieruchomosc13=new Nieruchomosc(220,"Ulica Słowackiego",150,Color.RED);
        nieruchomosc13.setCzynsz(18);
        nieruchomosc13.setId(23);
        listaNieruchomosci[12]=nieruchomosc13;
        pola[23]=nieruchomosc13;
        Nieruchomosc nieruchomosc14=new Nieruchomosc(240,"Plac Wilsona",150,Color.RED);
        nieruchomosc14.setCzynsz(20);
        nieruchomosc14.setId(24);
        pola[24]=nieruchomosc14;
        listaNieruchomosci[13]=nieruchomosc14;
        Obiekt obiekt4=new Obiekt("Dworzec", dworzec, 200, "Wschodni");
        dworce[2]=obiekt4;
        obiekt4.setId(25);
        listaObiektow[3]=obiekt4;
        pola[25]=obiekt4;
        Nieruchomosc nieruchomosc15=new Nieruchomosc(260,"Ulica Świętokrzyska",150,Color.YELLOW);
        nieruchomosc15.setCzynsz(22);
        nieruchomosc15.setId(26);
        listaNieruchomosci[14]=nieruchomosc15;
        pola[26]=nieruchomosc15;
        Nieruchomosc nieruchomosc16=new Nieruchomosc(260,"Krakowskie Przedmieście",150,Color.YELLOW);
        nieruchomosc16.setCzynsz(22);
        nieruchomosc16.setId(27);
        listaNieruchomosci[15]=nieruchomosc16;
        pola[27]=nieruchomosc16;
        Obiekt obiekt5=new Obiekt("Wodociągi", wodociagi, 150, "");
        obiekty1[1]=obiekt5;
        obiekt5.setId(28);
        listaObiektow[4]=obiekt5;
        pola[28]=obiekt5;
        Nieruchomosc nieruchomosc17=new Nieruchomosc(280,"Nowy Świat",150,Color.YELLOW);
        nieruchomosc17.setCzynsz(24);
        nieruchomosc17.setId(29);
        listaNieruchomosci[16]=nieruchomosc17;
        pola[29]=nieruchomosc17;
        Nieruchomosc nieruchomosc18=new Nieruchomosc(300,"Plac Trzech Krzyży",200,Color.GREEN);
        nieruchomosc18.setCzynsz(26);
        nieruchomosc18.setId(31);
        listaNieruchomosci[17]=nieruchomosc18;
        pola[31]=nieruchomosc18;
        Nieruchomosc nieruchomosc19=new Nieruchomosc(300,"Ulica Marszałkowska",200,Color.green);
        nieruchomosc19.setCzynsz(26);
        nieruchomosc19.setId(32);
        listaNieruchomosci[18]=nieruchomosc19;
        pola[32]=nieruchomosc19;
        Nieruchomosc nieruchomosc20=new Nieruchomosc(320,"Aleje Jerozolimskie",200,Color.green);
        nieruchomosc20.setCzynsz(28);
        nieruchomosc20.setId(34);
        listaNieruchomosci[19]=nieruchomosc20;
        pola[34]=nieruchomosc20;
        Obiekt obiekt6=new Obiekt("Dworzec", dworzec, 200, "Centralny");
        dworce[3]=obiekt6;
        obiekt6.setId(35);
        listaObiektow[5]=obiekt6;
        pola[35]=obiekt6;
        Nieruchomosc nieruchomosc21=new Nieruchomosc(350,"Ulica Belwederska",200,Color.CYAN);
        nieruchomosc21.setCzynsz(35);
        nieruchomosc21.setId(37);
        listaNieruchomosci[20]=nieruchomosc21;
        pola[37]=nieruchomosc21;
        Podatek podatek2=new Podatek(podatek, 150);
        podatek2.setOpis("domiar podatkowy");
        podatki[1]=podatek2;
        pola[38]=podatek2;
        Nieruchomosc nieruchomosc22=new Nieruchomosc(400,"Aleje Ujazdowskie",200,Color.cyan);  
        nieruchomosc22.setCzynsz(50);
        nieruchomosc22.setId(39);
        listaNieruchomosci[21]=nieruchomosc22;
        pola[39]=nieruchomosc22;
        for (int i = 0; i < 8; i++) {
          tablicaGrup.add(new Dzielnica());
        }
        tablicaGrup.get(0).setDzialki(new ArrayList(){{add(listaNieruchomosci[0]); add(listaNieruchomosci[1]);}});
        tablicaGrup.get(1).setDzialki(new ArrayList(){{add(listaNieruchomosci[2]); add(listaNieruchomosci[3]); add(listaNieruchomosci[4]);}});
         
        tablicaGrup.get(2).setDzialki(new ArrayList(){{add(listaNieruchomosci[5]); add(listaNieruchomosci[6]); add(listaNieruchomosci[7]);}});

        tablicaGrup.get(3).setDzialki(new ArrayList(){{add(listaNieruchomosci[8]); add(listaNieruchomosci[9]); add(listaNieruchomosci[10]);}});

        tablicaGrup.get(4).setDzialki(new ArrayList(){{add(listaNieruchomosci[11]); add(listaNieruchomosci[12]); add(listaNieruchomosci[13]);}});
 
        tablicaGrup.get(5).setDzialki(new ArrayList(){{add(listaNieruchomosci[14]); add(listaNieruchomosci[15]); add(listaNieruchomosci[16]);}});

        tablicaGrup.get(6).setDzialki(new ArrayList(){{add(listaNieruchomosci[17]); add(listaNieruchomosci[18]); add(listaNieruchomosci[18]);}});

        tablicaGrup.get(7).setDzialki(new ArrayList(){{add(listaNieruchomosci[20]); add(listaNieruchomosci[21]);}});
        grupyObiektow[0]=dworce;
        grupyObiektow[1]=obiekty1;

        
        
    
}
}
