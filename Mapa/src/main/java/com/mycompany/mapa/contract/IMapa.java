/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.mapa.contract;

import com.mycompany.bazakart.contracts.IBaza;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.obiekt.Obiekt;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.podatek.Podatek;
import com.mycompany.pole.Pole;
import com.mycompany.start.contracts.IStart;
import com.mycompany.wiezienie.contracts.IWiezienie;
import java.awt.Image;
import java.util.List;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Joanna667
 */
public interface IMapa {
    Pole[] getPola();
    Obiekt[] getObiekty();
    INieruchomosc[][] getGrupyObiektow();
    Nieruchomosc[] getDzialki();
    List<Dzielnica> getGrupy();
    Podatek[] getPodatki();
    IBaza getSzansa();
    void setSzansa(IBaza szansa);
    void setKasaSpoleczna(IBaza kasapol);
    IBaza getKasaSpoleczna();
    IWiezienie getWiezienie();
    IStart getStart();
    String getNazwa();
    void setNazwa(String nazwa);
    Image getLogo();
    //void setContext(BundleContext c);
}
