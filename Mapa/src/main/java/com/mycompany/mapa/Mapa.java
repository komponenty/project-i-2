/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.mapa;

import com.mycompany.bazakart.contracts.IBaza;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.idzdowiezienia.IdzDoWiezienia;
import com.mycompany.mapa.contract.IMapa;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.obiekt.Obiekt;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.parking.Parking;
import com.mycompany.podatek.Podatek;
import com.mycompany.pole.Pole;
import com.mycompany.start.Start;
import com.mycompany.start.contracts.IStart;
import com.mycompany.wiezienie.Wiezienie;
import com.mycompany.wiezienie.contracts.IWiezienie;
import java.awt.Image;
import java.util.List;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;

/**
 *
 * @author Joanna667
 */
public class Mapa implements IMapa {
    protected Pole[] pola;
    protected Podatek[] podatki;
    protected Nieruchomosc[] listaNieruchomosci;
    protected Obiekt[] listaObiektow;
    protected List<Dzielnica> tablicaGrup;
    protected IBaza szansa;
    protected IBaza kasaspol;
    protected Wiezienie wiezienie;
    protected Parking parking;
    protected IdzDoWiezienia idzdowiezienia;
    protected Start start;
    protected String nazwa;
    protected Image logo;
    protected INieruchomosc[][] grupyObiektow;
    protected BundleContext context;
    public Mapa(String title) throws InvalidSyntaxException{
        this.nazwa=title;
        //puste tablice
        this.pola=new Pole[40];
        this.listaObiektow=new Obiekt[6];
        this.listaNieruchomosci=new Nieruchomosc[22];
        this.podatki=new Podatek[2];
 
    }

    public Podatek[] getPodatki() {
        return podatki;
    }

    public void setPodatki(Podatek[] zestaw) {
        this.podatki=zestaw;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String s) {
       this.nazwa=s;
    }

    public Nieruchomosc[] getDzialki() {
        return listaNieruchomosci;
    }

    public void setDzialki(Nieruchomosc[] zestaw) {
       this.listaNieruchomosci=zestaw;
    }
     public Obiekt[] getObiekty() {
        return listaObiektow;
    }

    public void setObiekty(Obiekt[] zestaw) {
       this.listaObiektow=zestaw;
    }

    public Pole[] getPola() {
       return pola;
    }

    public void setPola(Pole[] zestaw) {
       this.pola=zestaw;
    }

    @Override
    public List<Dzielnica> getGrupy() {
        return tablicaGrup;
    }

    public IBaza getSzansa() {
       return szansa;
    }

    public IBaza getKasaSpoleczna() {
       return kasaspol;
    }

    public INieruchomosc[][] getGrupyObiektow() {
        return grupyObiektow;
    }

    public Image getLogo() {
       return logo;
    }

//    public void setContext(BundleContext c) {
//        this.context=c;
//    }

    public IWiezienie getWiezienie() {
        return wiezienie;
    }

    public IStart getStart() {
       return start;
    }

    public void setSzansa(IBaza szansa) {
        this.szansa=szansa;
    }

    public void setKasaSpoleczna(IBaza kasa) {
        this.kasaspol=kasa;
    }


}
