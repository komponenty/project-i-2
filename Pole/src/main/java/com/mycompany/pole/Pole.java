/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.pole;

import com.mycompany.pole.contract.IPole;
import java.awt.*;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Pole implements IPole {
    protected int id;
    protected Image ikona;
    protected String tytul;
    protected String typ;
    protected String opis;
    
    protected Point pozycja;
    
    public Pole(){
        this.typ="";
        this.tytul="Pole planszy";
        this.opis="pole";
        this.ikona=null;
    }



    public String getNazwa() {
       return tytul;
    }

    public int getId() {
        return id;
               
    }

    public void setId(int i) {
       this.id=i;
    }

    public void setIcon(Image im) {
       this.ikona=im;
    }

    public Image getIcon() {
       return ikona;
    }

    @Override
    public String getOpis() {
       return opis;
    }

    public Point getPozycja() {
        return pozycja;
    }

    public void setPozycja(Point p) {
        this.pozycja=p;
    }
    @Override
    public String toString(){
        return this.typ+this.tytul;
    }

    public void setOpis(String s) {
        this.opis=s;
    }


    
    
}
