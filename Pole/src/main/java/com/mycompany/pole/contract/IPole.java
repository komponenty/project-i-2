/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.pole.contract;

import java.awt.*;

/**
 *
 * @author Joanna667
 */
public interface IPole {
    
    String getNazwa();
    String getOpis();
    void setOpis(String s);
    int getId();
    void setId(int i);
    void setIcon(Image i);
    Image getIcon();
    Point getPozycja();
    void setPozycja(Point p);
    @Override
    String toString();
    
}
