package com.mycompany.parking;

import com.mycompany.pole.Pole;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Parking parking;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
       this.parking=new Parking();
       this.context=context;
       context.registerService(Pole.class.getName(), this.parking, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
