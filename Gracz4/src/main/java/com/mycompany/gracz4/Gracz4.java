/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.gracz4;

import com.mycompany.gracz.Gracz;
import com.mycompany.pionek.Pionek;
import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Gracz4 extends Gracz{
   public Gracz4(){
        super();
        this.imie="Gracz4";
        Image ikonka = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/gracz4/pionek4.png")).getImage();
        this.pionek.setIkonka(ikonka);
        this.pionek.setNazwa("4");
        this.pionek.setNumer(4);
        
    } 
}
