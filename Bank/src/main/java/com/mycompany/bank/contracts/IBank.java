/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.bank.contracts;

import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.mapa.contract.IMapa;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.contracts.INieruchomosc;
import com.mycompany.pole.contract.IPole;
import java.util.List;

/**
 *
 * @author Joanna667
 */
public interface IBank {
    void wplata(IGracz klient, int kwota);
    void wyplata(IGracz klient, int kwota);
    void kupNieruchomosc(IGracz klient, int id);
   // void setObiekty(INieruchomosc[][] nn);
   // void setDzialki(Dzielnica[] d);
   // void aktualizujOferte();
    void pobierzMape(IMapa mapa);
    void setOferta(int index, INieruchomosc n);
    INieruchomosc getOferta(int index);
    List<INieruchomosc> getOferta();
    void zwrotNieruchomosci(INieruchomosc nieruchomosc);
    boolean zezwolenieNaBudowe(IGracz gracz, INieruchomosc dzialka);
    void konfiskujMajatek(IGracz gracz);
    
    
}
