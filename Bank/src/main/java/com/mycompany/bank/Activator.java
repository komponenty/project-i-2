package com.mycompany.bank;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Bank bank;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.bank=new Bank();
        this.context=context;
        context.registerService(Bank.class.getName(), this.bank, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
