/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.bank;

import com.mycompany.bank.contracts.IBank;
import com.mycompany.dzielnica.Dzielnica;
import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.mapa.contract.IMapa;
import com.mycompany.nieruchomosc.Nieruchomosc;
import com.mycompany.nieruchomosc.contracts.IDzialka;
import com.mycompany.obiekt.Obiekt;
import com.mycompany.obiekt.contracts.INieruchomosc;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Joanna667
 */
public class Bank implements IBank {
    List<INieruchomosc> oferta;
    Dzielnica[] dzielnice;
    INieruchomosc[][] obiekty;
    
    protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Bank(){
        this.dzielnice=new Dzielnica[8];
        this.obiekty=new INieruchomosc[2][];
        this.oferta=new ArrayList<INieruchomosc>(){};
    }

    public void wplata(IGracz klient, int kwota) {
        
        klient.setSrodki(klient.getSrodki()+kwota);
    }

    public void wyplata(IGracz klient, int kwota) {
        klient.setSrodki(klient.getSrodki()-kwota);
    }
    private boolean znajdzNieruchomosc(int id){
         for (INieruchomosc dzialka:oferta) {
             if (dzialka.getId()==id) {
                 this.oferta.remove(dzialka);
                return true;
             }
        } 
         return false;
    }
    public void kupNieruchomosc(IGracz klient, int id) {
        if (znajdzNieruchomosc(id)==false) {
            return;
        }
        for (Dzielnica dzielnia : dzielnice) {
            List<IDzialka> dzialki=dzielnia.getDzialki();
            for (IDzialka d1 : dzialki) {
                IDzialka dzialka = d1;
                if (d1.getId() == id && d1.getStatus() == false) {
                    d1.zakup(klient);
                    klient.dodajNieruchomosc(d1);
                    this.wyplata(klient, d1.getCena());
                    //wbrew nazwie to jest sprawdzenie, monopol jest ustawiany tylko gdy gracz ma już resztę dzielnicy
                    //w przeciwnym razie własciciel dzielnicy jest nadal null
                    klient.setMonopol(dzielnia);
                    //oferta.remove(id);
                    this.pcs.fireIndexedPropertyChange("oferta", id, dzialka, null);
                }

                
            }
           
        }
        for (INieruchomosc[] grupa : obiekty) {
            INieruchomosc[] dzialki=grupa;
            for (INieruchomosc d1 : grupa) {
                if (d1.getId() == id && d1.getStatus() == false) {
                    INieruchomosc obiekt = d1;
                    d1.zakup(klient);
                    this.wyplata(klient, d1.getCena());
                    klient.dodajObiekt((INieruchomosc) d1);
                    this.pcs.fireIndexedPropertyChange("oferta", id, obiekt, null);
                }
                
            }
        }
        
    }
//    public void setObiekty(INieruchomosc[][] oo) {
//        this.obiekty=oo;
//    }
//    public INieruchomosc[][] getObiekty() {
//       return obiekty;
//    }
    public List<INieruchomosc> getOferta(){
      
        return oferta;
    }

//    public void setDzialki(Dzielnica[] d) {
//       this.dzielnice=d;
//    }

    public void zwrotNieruchomosci(INieruchomosc nieruchomosc) {
       nieruchomosc.oddaj();
       this.oferta.add(nieruchomosc);
    }

    public void aktualizujOferte() {
        List<INieruchomosc> oferta1 = this.oferta;
       
        for (int i = 0; i < dzielnice.length; i++) {
            for (int j = 0; j < dzielnice[i].getDzialki().size(); j++) {
                //this.oferta[dzielnice[i].getDzialki().get(j).getId()]=dzielnice[i].getDzialki().get(j);
                this.oferta.add(dzielnice[i].getDzialki().get(j));
            }
        }
        for (int i = 0; i < obiekty.length; i++) {
            for (int j = 0; j < obiekty[i].length; j++) {
                //this.oferta[obiekty[i][j].getId()]=obiekty[i][j];
                this.oferta.add(obiekty[i][j]);
            }
        }
        this.pcs.firePropertyChange("oferta", oferta1, this.oferta);
    }
    //oferta jest właściwością indeksowaną
    public void setOferta(int index, INieruchomosc n) {
        INieruchomosc n1=this.oferta.get(index);
        this.oferta.set(index, n);
        this.pcs.fireIndexedPropertyChange("oferta", index, n1, n);
    }

    public INieruchomosc getOferta(int index) {
        return oferta.get(index);
    }

    public boolean zezwolenieNaBudowe(IGracz gracz, INieruchomosc dzialka) {
        int i = dzialka.getId();
        for (Dzielnica dzielnia : dzielnice) {
            List<IDzialka> dzialki=dzielnia.getDzialki();
            if (dzielnia.getWlasciciel().equals(gracz.getImie())) {
                //szukamy - jak nie znajdziemy znaczy że działki nie ma w tej dzielnicy
                for (IDzialka d:dzialki) {
                    if (d.getId()==dzialka.getId()) {
                        return true;
                    }
                }
 
            }
        }
        //jeśli dzialki nie ma w żadnej dzielnicy - oznacza że nie jest to działka budowlana
        return false;
    }

    public void konfiskujMajatek(IGracz gracz) {
        for (int i = 0; i < gracz.getObiekty().size(); i++) {
            gracz.getObiekty().get(i).oddaj();
            this.zwrotNieruchomosci(gracz.getObiekty().get(i));
        }
    }

    public void pobierzMape(IMapa mapa) {
        int len= mapa.getGrupy().size();
        this.dzielnice=mapa.getGrupy().toArray(new Dzielnica[len]);
        this.obiekty=mapa.getGrupyObiektow();
        this.aktualizujOferte();
    }
    
}
