/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.wiezienie.contracts;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.pole.contract.IPole;
import java.util.List;


/**
 *
 * @author st
 */
public interface IWiezienie extends IPole {
    List<IGracz> getWiezniowie();
    boolean Aresztuj(IGracz g);
    void Uwolnij(IGracz g);
}
