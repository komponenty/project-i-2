/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.wiezienie;

import com.mycompany.gracz.contracts.IGracz;
import com.mycompany.gracz.contracts.IWiezien;
import com.mycompany.pole.Pole;
import com.mycompany.wiezienie.contracts.IWiezienie;
import java.awt.Image;
import java.util.*;
import java.beans.PropertyChangeSupport;
import javax.swing.ImageIcon;

/**
 *
 * @author Joanna667
 */
public class Wiezienie extends Pole implements IWiezienie {
    //lista jest po to tylko żeby móc sprawdzić z jakiego powodu gracz jest nieaktywny
    private List<IGracz> wiezniowie;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public Wiezienie() {
        super();
        this.wiezniowie=new ArrayList<IGracz>(){};
        this.ikona=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/wiezienie/grafika/siedzi1.png")).getImage();;
        this.tytul="Więzienie";
        this.opis="Jeśli trafiłeś na to pole w wyniku zwykłego ruchu, znaczy że jesteś tylko odwiedząjącym, idź dalej";

    }
    //został złapany jeśli ma immunitet to nie idzie do więzienia
    public boolean Aresztuj(IGracz g) {
        int i = g.getImmunitet();
        g.setImmunitet(i-1);
        if(i<=0){
         ((IWiezien)g).setStatus(false);
         ((IWiezien)g).setKara(3);
          this.wiezniowie.add(g);
          return true;
        }
        return false;
    }

    public void Uwolnij(IGracz g) {
       ((IWiezien)g).setStatus(true);
       this.wiezniowie.remove(g);
    }

    public List<IGracz> getWiezniowie() {
        return wiezniowie;
    }
    
}
