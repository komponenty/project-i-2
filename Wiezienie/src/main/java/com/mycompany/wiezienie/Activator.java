package com.mycompany.wiezienie;

import com.mycompany.pole.Pole;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Wiezienie wiezienie;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
       this.wiezienie=new Wiezienie();
       this.context=context;
       context.registerService(Pole.class.getName(), this.wiezienie, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
