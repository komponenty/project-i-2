/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.about.contract;

/**
 *
 * @author Joanna667
 */
public interface IPobierzInfo {
     void setAuthors(String autor1, String autor2, String autor3);
     String getVersion();
     void setVersion(String wersja);
     void show();
}
