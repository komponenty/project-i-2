/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.about;

import com.mycompany.about.contract.*;
import javax.swing.*;

/**
 *
 * @author Joanna667
 */
public class About implements IPobierzInfo {
    private String author1;
    private String author2;
    private String author3;
    private String v;
    public About(){
       
    }
    public void setAuthors(String autor1, String autor2, String autor3) {
        this.author1=autor1;
        this.author2=autor2;
        this.author3=autor3;
    }
    
    public String getVersion() {
        return v;
    }
    public void setVersion(String nowa){
        this.v=nowa;
    }
    
    public void show(){
         JOptionPane.showMessageDialog(null, "Autorzy: "+author1+", "+author2+", "+author3+". Wersja: "+v, "Informacje o programie", JOptionPane.INFORMATION_MESSAGE);
    }

 

    
    
}
