package com.mycompany.about;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private BundleContext context;
    private About komponent;
    public void start(BundleContext context) throws Exception {
        this.komponent=new About();
        this.context=context;
        context.registerService(About.class.getName(), this.komponent, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
