/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.pionek.contracts;

import java.awt.Image;
import java.awt.Point;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Joanna667
 */
public interface IPionek{
    void setIkonka(Image ikona);
    Image getIkonka();
    int getPozycja();
    void setPozycja(int i);
    String getNazwa();
    void setNazwa(String n);
    int getNumer();
    void setNumer(int i);

   void setLocation(int i, int i0);
            
    
}
