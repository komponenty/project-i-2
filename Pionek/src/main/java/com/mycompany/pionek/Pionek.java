/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.pionek;

import com.mycompany.pionek.contracts.*;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.*;

/**
 *
 * @author Joanna667
 */
public class Pionek extends JButton implements IPionek{
    private Image ikonka;
    private int pozycja;
    private String nazwa;
    private int numer;
    public Pionek(){
        super();
        this.ikonka=new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/pionek/grafika/empty.png")).getImage();
        
        this.setPreferredSize(new Dimension(10, 10));
        this.nazwa="";
        this.numer=0;
        this.pozycja=0;
        this.setVisible(true);
    }
    public void setIkonka(Image ikona){
        ImageIcon im = new ImageIcon(ikona);
        if(ikona==null)
        {
            this.setText(nazwa);
        }
        else
        {
            this.setIcon(im);
        }
        this.ikonka=ikona;
         
    }
    public int getPozycja() {
       return pozycja;
    }

    public Image getIkonka() {
        return ikonka;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String n) {
        this.nazwa=n;
    }

    public void setPozycja(int i) {
        this.pozycja=i;
    }

    public int getNumer() {
       return numer;
    }

    public void setNumer(int i) {
       this.numer=i;
    }

    @Override
    public void setLocation(int x,int y){
      super.setLocation(x, y);
    }



            
   
    
}

