package com.mycompany.pionek;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Pionek pionek;
    private BundleContext context;
    public void start(BundleContext context) throws Exception {
        this.pionek=new Pionek();
        this.context=context;
        context.registerService(Pionek.class.getName(), this.pionek, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
