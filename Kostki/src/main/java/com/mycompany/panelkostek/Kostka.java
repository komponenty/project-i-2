/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.panelkostek;

import com.mycompany.panelkostek.contracts.IKostka;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Joanna667
 */
public class Kostka extends JButton implements IKostka{
    private ImageIcon image;
    private int oczka;
    private final ImageIcon[] wartosci;
    public Kostka(){
        this.wartosci=new ImageIcon[6];
        Image p1 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/1.png")).getImage();
        Image p2 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/2.png")).getImage();
        Image p3 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/3.png")).getImage();
        Image p4 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/4.png")).getImage();
        Image p5 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/5.png")).getImage();
        Image p6 = new ImageIcon(this.getClass().getClassLoader().getResource("com/mycompany/panelkostek/grafika/6.png")).getImage();
        wartosci[0]=new ImageIcon(p1);
        wartosci[1]=new ImageIcon(p2);
        wartosci[2]=new ImageIcon(p3);
        wartosci[3]=new ImageIcon(p4);
        wartosci[4]=new ImageIcon(p5);
        wartosci[5]=new ImageIcon(p6);
        
        this.setSize(new Dimension(40,40));
        this.setLayout(new GridLayout(1,1));
        this.setIcon(wartosci[0]);
        this.oczka=1;
        this.setOpaque(true);
        this.setVisible(true);
        
        
    }
    public ImageIcon getImage(){
        return image;
    }

    public void Rzucaj() {
        Random r = new Random();
        int i = r.nextInt(5);
        this.oczka=i+1;
        if(wartosci[i+1]!=null){
            this.image=wartosci[i];
           this.setIcon(wartosci[i]);}
        else{
            this.image=null;
           this.setText(" "+oczka+" ");
           
        }
        
        
    }

    public int getOczka() {
        return oczka;
    }
    
}

