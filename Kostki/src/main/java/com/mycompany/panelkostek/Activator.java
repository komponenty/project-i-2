package com.mycompany.panelkostek;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private Kostki panel;
    private BundleContext context;     
    public void start(BundleContext context) throws Exception {
        this.panel=new Kostki();
        this.context=context;
        context.registerService(Kostki.class.getName(), this.panel, null);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
