/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.panelkostek.contracts;

/**
 *
 * @author Joanna667
 */
public interface IKostki {
    int getOczka();
    void setStatus(boolean b);
    boolean getStatus();
    String getKomunikat();
    boolean dublet();
}
